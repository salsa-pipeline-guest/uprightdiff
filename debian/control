Source: uprightdiff
Section: devel
Priority: optional
Maintainer: MediaWiki packaging team <mediawiki-debian@lists.wikimedia.org>
Uploaders: Kunal Mehta <legoktm@debian.org>
Build-Depends: debhelper (>= 10), libopencv-highgui-dev, libopencv-imgcodecs-dev,
 libboost-program-options-dev
Standards-Version: 4.3.0
Homepage: https://www.mediawiki.org/wiki/Uprightdiff
Vcs-Git: https://salsa.debian.org/mediawiki-team/uprightdiff.git
Vcs-Browser: https://salsa.debian.org/mediawiki-team/uprightdiff
Rules-Requires-Root: no

Package: uprightdiff
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: examine differences between two images
 This utility examines the differences between two images.
 It produces a visual annotation and reports statistics.
 .
 It is optimised for images which come from browser screenshots.
 It analyses the image for vertical motion, and annotates connected
 regions that have the same vertical displacement. Then it highlights
 any remaining ("residual") differences which are not explained by
 vertical motion on a pixel-by-pixel basis.
